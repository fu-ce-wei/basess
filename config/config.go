package config

import (
	"github.com/spf13/viper"
)

type Config struct {
	Port     int    `mapstructure:"port"`
	Mode     string `mapstructure:"mode"`
	LogLevel string `mapstructure:"logLevel"`
}

func LoadEnv(path string) (config Config, err error) {
	if path != "" {
		viper.SetConfigFile(path)
	} else {
		viper.SetConfigType("yaml")
		viper.SetConfigName("settings-dev")
		viper.AddConfigPath(".")
	}

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)

	return config, nil
}
