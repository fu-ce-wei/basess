package cmd

import (
	"errors"
	"fmt"
	"gitee.com/fu-ce-wei/basess/cmd/config"
	"gitee.com/fu-ce-wei/basess/cmd/version"
	"github.com/spf13/cobra"
	"os"
)

var rootCmd = &cobra.Command{
	Use:          "main",
	Short:        "main",
	SilenceUsage: true,
	Long:         `main`,
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			tip()
			return errors.New("parameter error")
		}
		return nil
	},
	PersistentPreRunE: func(*cobra.Command, []string) error { return nil },
	Run: func(cmd *cobra.Command, args []string) {
		tip()
	},
}

func tip() {
	usageStr := ` 🚀 Can use` + `-h` + ` View command`
	fmt.Printf("%s\n", usageStr)

}

func init() {
	rootCmd.AddCommand(version.StartCmd)
	rootCmd.AddCommand(config.StartCmd)

}

// Execute : apply commands
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(-1)
	}
}
